import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavbarMenu from './components/Navbar';
import Banner from './components/Banner'
import Content from './components/Content';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <NavbarMenu></NavbarMenu>
      <Banner></Banner>
      <Content></Content>
      <Footer></Footer>
    </div>
  );
}

export default App;
