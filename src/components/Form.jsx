
const FormMenu = () => (
    <section className="cars-service mt-8">
  <div className="container cars-form bg-white rounded rounded-3">
    <form className="d-flex flex-wrap align-items-end justify-content-between">
      <div className="p-2 form-item">
        <label htmlFor="tipe" className="form-label">Tipe Driver</label>
        <div className="input-group">
          <input type="text" className="form-control" id="tipe" aria-describedby="emailHelp" placeholder="Pilih Tipe Driver" />
          <span className="input-group-text">
            <img src="assets/icons/icon_arr_up.svg" alt="Panah" className="inp-arr" />
          </span>
          <div className="input-option bg-white" id="tipe_driver--option">
            <p className="fs-6 p-2 mb-2 input-item">Dengan Sopir</p>
            <p className="fs-6 p-2 mb-2  input-item">Tanpa Sopir (Lepas Kunci)</p>
          </div>
        </div>
      </div>
      <div className="p-2 form-item">
        <label htmlFor="tanggal" className="form-label">Tanggal</label>
        <div className="input-group">
          <input type="date" className="form-control" id="tanggal" />
        </div>
      </div>
      <div className="p-2 form-item">
        <label htmlFor="waktu" className="form-label">Waktu Jemput/Ambil</label>
        <div className="input-group">
          <input type="text" className="form-control" id="waktu" autoComplete="off" placeholder="Pilih Waktu" />
          <span className="input-group-text">
            <img src="assets/icons/icon_clock.svg" alt="Icon Jam" className="clock" />
          </span>
          <div className="input-option bg-white waktu-form" id="tipe_waktu--option">
            <p className="fs-6 p-2 mb-2 waktu-item">08:00 WIB</p>
            <p className="fs-6 p-2 mb-2 waktu-item">09:00 WIB</p>
            <p className="fs-6 p-2 mb-2 waktu-item">10:00 WIB</p>
            <p className="fs-6 p-2 mb-2 waktu-item">11:00 WIB</p>
            <p className="fs-6 p-2 mb-2 waktu-item">12:00 WIB</p>
          </div>
        </div>
      </div>
      <div className="p-2 form-item">
        <label htmlFor="jumlah" className="form-label">Jumlah Penumpang</label>
        <div className="input-group">
          <input type="number" className="form-control" id="jumlah" autoComplete="off" min={1} max={9} placeholder="Jumlah Penumpang" />
          <span className="input-group-text">
            <img src="assets/icons/icon_capacity.svg" alt="Icon Penumpang" className="inp-psg" />
          </span>
        </div>
      </div>
      <div className="p-1 form-item mb-1">
        <button type="button" className="btn btn-success" id="cari">Cari Mobil</button>
      </div>
    </form>
  </div>
  <div className="container cars-avail pb-4">
    <div className="car-container row gx-lg-5 px-0 gy-4" style={{marginTop: '-40px'}}>
    </div>
  </div>
</section>

)

export default FormMenu;