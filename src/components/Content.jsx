
const Content = () => (
    <div>
  <section className="content h-100vh" id="ourservices">
    <div className="container-content-2">
      <div className="row justify-content-center">
        <div className="col-5 img-service">
          <img src="assets/images/human.png" alt="Image" />
        </div>
        <div className="col-4 desc">
          <p className="title2">Best Car Rental for any kind of trip in (Lokasimu)!</p>
          <p className="description2">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
          <p className="list mb-4"><img src="assets/images/list.png" alt="List" />Sewa Mobil Dengan Supir di Bali 12 Jam</p>
          <p className="list mb-4"><img src="assets/images/list.png" alt="List" />Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
          <p className="list mb-4"><img src="assets/images/list.png" alt="List" />Sewa Mobil Jangka Panjang Bulanan</p>
          <p className="list mb-4"><img src="assets/images/list.png" alt="List" />Gratis Antar - Jemput Mobil di Bandara</p>
          <p className="list mb-4"><img src="assets/images/list.png" alt="List" />Layanan Airport Transfer / Drop In Out</p>
        </div>
      </div>
    </div>
  </section>
  <section className="whyus h-100vh" id="whyus">
    <div className="why-us">
      <div className="container">
        <div className="text-why-us">
          <p className="title-why-us">Why Us?</p>
          <p className="desc-why-us">Mengapa harus pilih Binar Car Rental?</p>
        </div>
        <div className="row cards-2">
          <div className="col-md-3 cards-3">
            <div className="card-why-us border">
              <div className="gallery-photo">
                <img src="assets/images/icon-like.png" alt className="img-responsive" />
              </div>
              <div className="texting">
                <p className="title-why-us">Mobil Lengkap</p>
                <p className="desc-why-us-2">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
              </div>
            </div>
          </div>
          <div className="col-md-3 cards-3">
            <div className="card-why-us border">
              <div className="gallery-photo">
                <img src="assets/images/icon-price.png" alt className="img-responsive" />
              </div>
              <div className="texting">
                <p className="title-why-us">Harga Murah</p>
                <p className="desc-why-us-2">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
              </div>
            </div>
          </div>
          <div className="col-md-3 cards-3">
            <div className="card-why-us border">
              <div className="gallery-photo">
                <img src="assets/images/icon-clock.png" alt className="img-responsive" />
              </div>
              <div className="texting">
                <p className="title-why-us">Layanan 24 Jam</p>
                <p className="desc-why-us-2">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
              </div>
            </div>
          </div>
          <div className="col-md-3 cards-3">
            <div className="card-why-us border">
              <div className="gallery-photo">
                <img src="assets/images/icon-award.png" alt className="img-responsive" />
              </div>
              <div className="texting">
                <p className="title-why-us">Sopir Profesional</p>
                <p className="desc-why-us-2">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section className="testi h-100vh" id="testimonial">
    <div className="testimonial">
      <div className="container">
        <div className="text-testimonial text-center">
          <p className="title-testimonial">Testimonial</p>
          <p className="desc-testimonial">Berbagai review positif dari para pelanggan kami</p>
        </div>
      </div>
    </div>
  </section>
  <section className="pt-1 pb-1 carousel-testimonial">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div id="carouselExampleIndicators2" set data-interval="false" className="carousel slide" data-ride="carousel" style={{width: '100%'}}>
            <div className="carousel" style={{width: '100%'}}>
              <div className="carousel-item active">
                <div className="row">
                  <div className="col-md-4 mb-3">
                    <div className="card holder card-1" style={{marginLeft: '-385px'}}>
                      <div className="row">
                        <div className="col-md-6 photo1">
                          <img src alt />
                        </div>
                        <div className="col-md-6 text1 star-1">
                          <img className="star mb-1 mt-2" src="assets/images/star.png" alt />
                          <div className="text-container-carousel text-1">
                            <p className="text-carousel text-left">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p className="text-carousel2 text-left">John Dee 32, Bromo</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 mb-3">
                    <div className="card holder card-2">
                      <div className="row">
                        <div className="col-md-6 photo1">
                          <img src="assets/images/img-photo1.png" alt />
                        </div>
                        <div className="col-md-6 text1">
                          <img className="star mb-1 mt-2" src="assets/images/star.png" alt style={{marginLeft: '-42px'}} />
                          <div className="text-container-carousel">
                            <p className="text-carousel text-left">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p className="text-carousel2 text-left">John Dee 32, Bromo</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 mb-3">
                    <div className="card holder card-3">
                      <div className="row">
                        <div className="col-md-6 photo1">
                          <img src="assets/images/img-photo2.png" alt />
                        </div>
                        <div className="col-md-6 text1">
                          <img className="star mb-1 mt-2" src="assets/images/star.png" alt style={{marginLeft: '-42px'}} />
                          <div className="text-container-carousel">
                            <p className="text-carousel text-left">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p className="text-carousel2 text-left">John Dee 32, Bromo</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row">
                  <div className="col-md-4 mb-3 slider1">
                    <div className="card holder card-1" style={{marginLeft: '-385px'}}>
                      <div className="row">
                        <div className="col-md-6 photo1">
                          <img src alt />
                        </div>
                        <div className="col-md-6 text1 star-1">
                          <img className="star mb-1 mt-2" src="assets/images/star.png" alt />
                          <div className="text-container-carousel text-1">
                            <p className="text-carousel text-left">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p className="text-carousel2 text-left">John Dee 32, Bromo</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 mb-3 slider2">
                    <div className="card holder card-2">
                      <div className="row">
                        <div className="col-md-6 photo1">
                          <img src="assets/images/img-photo1.png" alt />
                        </div>
                        <div className="col-md-6 text1">
                          <img className="star mb-1 mt-2" src="assets/images/star.png" alt style={{marginLeft: '-42px'}} />
                          <div className="text-container-carousel">
                            <p className="text-carousel text-left">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p className="text-carousel2 text-left">John Dee 32, Bromo</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 mb-3 slider3">
                    <div className="card holder card-3">
                      <div className="row">
                        <div className="col-md-6 photo1">
                          <img src="assets/images/img-photo2.png" alt />
                        </div>
                        <div className="col-md-6 text1">
                          <img className="star mb-1 mt-2" src="assets/images/star.png" alt style={{marginLeft: '-42px'}} />
                          <div className="text-container-carousel">
                            <p className="text-carousel text-left">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p className="text-carousel2 text-left">John Dee 32, Bromo</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 text-center btn-carousel mt-3">
        <a className="btn mb-3 mr-1 left-button" href="#carouselExampleIndicators2" role="button" data-slide="prev">
          <img src="assets/images/Left-button.png" alt />
        </a>
        <a className="btn mb-3 right-button" href="#carouselExampleIndicators2" role="button" data-slide="next">
          <img src="assets/images/Right-button.png" alt />
        </a>
      </div>
    </div>
  </section>
  <section className="banner-cta h-100vh">
    <div className="cta-banner">
      <div className="cta text-center">
        <p className="title-cta">Sewa Mobil di (Lokasimu) Sekarang</p>
        <p className="desc-cta">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        <button type="button" className="btn btn-cta text-light">
          <p>Mulai Sewa Mobil</p>
        </button>
      </div>
    </div>
  </section>
  <section className="faq h-100vh" id="faq">
    <div className="question">
      <div className="container" style={{marginLeft: 136}}>
        <div className="row">
          <div className="col-sm-6">
            <p className="title-question">Frequently Asked Question</p>
            <p className="dec-question">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <div className="col-sm-6 list-faq">
            <div id="accordion" className="accordion-faq">
              <div className="card mb-2">
                <div className="cardss">
                  <a className="btn" data-bs-toggle="collapse" href="#collapseOne">
                    Apa saja syarat yang dibutuhkan?
                  </a>
                  <a className="btn" href><img src="assets/images/list-down.jpeg" alt style={{marginLeft: 70}} /></a>
                </div>
              </div>
              <div className="card mb-2">
                <div className="cardss">
                  <a className="collapsed btn" data-bs-toggle="collapse" href="#collapseTwo">
                    Berapa hari minimal sewa mobil lepas kunci?
                  </a>
                  <a className="btn" href><img src="assets/images/list-down.jpeg" style={{marginLeft: 3}} alt /></a>
                </div>
              </div>
              <div className="card mb-2">
                <div className="cardss">
                  <a className="collapsed btn" data-bs-toggle="collapse" href="#collapseThree">
                    Berapa hari sebelumnya sabaiknya booking sewa mobil?
                  </a>
                  <a className="btn" href><img src="assets/images/list-down.jpeg" style={{marginLeft: '-70px'}} alt /></a>
                </div>
              </div>
              <div className="card mb-2">
                <div className="cardss">
                  <a className="collapsed btn" data-bs-toggle="collapse" href="#collapseFour">
                    Apakah Ada biaya antar-jemput?
                  </a>
                  <a href><img src="assets/images/list-down.jpeg" style={{marginLeft: 94}} alt /></a>
                </div>
              </div>
              <div className="card mb-2">
                <div className="cardss">
                  <a className="collapsed btn" data-bs-toggle="collapse" href="#collapseFive">
                    Bagaimana jika terjadi kecelakaan
                  </a>
                  <a className="btn" href> <img src="assets/images/list-down.jpeg" style={{marginLeft: 71}} alt /></a>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

)

export default Content;