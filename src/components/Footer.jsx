

const Footer = () => (
    <section className="footer" id="footer">
  <div className="container-footer">
    <div className="row">
      <div className="col-md-3">
        <div className="text-footer1">
          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
          <p>binarcarrental@gmail.com</p>
          <p>081-233-334-808</p>
        </div>
      </div>
      <div className="col-md-3">
        <div className="text-footer2">
          <p><a href="#ourservices">Our services</a></p>
          <p><a href="#whyus">Why Us</a></p>
          <p><a href="#testimonial">Testimonial</a></p>
          <p><a href="#faq">FAQ</a></p>
        </div>
      </div>
      <div className="col-md-3">
        <div className="text-footer3">
          <p>Connect with us</p>
          <div className="icon-row d-flex">
            <div className="icon">
              <a href><img src="assets/images/icon_facebook.png" alt /></a>
            </div>
            <div className="icon">
              <a href><img src="assets/images/icon_instagram.png" alt /></a>
            </div>
            <div className="icon">
              <a href><img src="assets/images/icon_twitter.png" alt /></a>
            </div>
            <div className="icon">
              <a href><img src="assets/images/icon_mail.png" alt /></a>
            </div>
            <div className="icon">
              <a href><img src="assets/images/icon_twitch.png" alt /></a>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3">
        <div className="text-footer4">
          <p>Copyright Binar 2022</p>
          <img src="assets/images/rectangle.png" alt />
        </div>
      </div>
    </div>
  </div>
</section>

)

export default Footer;