
const NavbarMenu = () => (
    <nav className="navbar navbar-expand-sm bg-light navbar-light navbarAtas" style={{backgroundColor: '#F1F3FF !important'}}>
  <a className="navbar-brand" href="#" style={{marginLeft: 15}}>
    <img src="assets/images/logo.png" width height alt="Logo" />
  </a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
    <span className="navbar-toggler-icon" />
  </button>
  <div className="collapse navbar-collapse flex-grow-0 justify-content-end" id="navbarSupportedContent">
    <ul className="navbar-nav text-left mt-2">
      <li className="nav-item active ml-auto p-3">
        <a className="nav-link" href="#ourservices">Our&nbsp;Services</a>
      </li>
      <li className="nav-item ml-auto p-3">
        <a className="nav-link" href="#whyus">Why Us</a>
      </li>
      <li className="nav-item ml-auto p-3">
        <a className="nav-link" href="#testimonial">Testimonial</a>
      </li>
      <li className="nav-item ml-auto p-3">
        <a className="nav-link" href="#faq">FAQ</a>
      </li>
      <button className="btn btn-success ml-auto text-center mt-3" style={{marginRight: 15}}>Register</button>
    </ul>
  </div>
</nav>

)

export default NavbarMenu;