
const Banner = () => (
    <section className="banner h-100vh">
  <div className="container">
    <div className="d-flex flex-row justify-content-between flex-wrap h-100vh" style={{marginTop: 20, padding: 0}}>
      <div className="align-self-center">
        <h2 className="h2-1">Sewa &amp; Rental Mobil Terbaik</h2>
        <h2 className="h2-1 mb-3">di kawasan (Lokasimu)</h2>
        <p className="description">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas <br />terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk <br />sewa mobil selama 24 jam.</p>
        <button className="btn mt-3 text-light btn-success text-center"><a href="/Form" style={{textDecoration: 'none', color: 'white !important'}}>Mulai Sewa Mobil</a></button>
      </div>
      <div className="align-self-center mt-5 car">
        <img src="assets/images/car-1.png" alt className="img-responsive" />
      </div>
    </div>
  </div>
</section>

)

export default Banner;